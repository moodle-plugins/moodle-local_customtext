// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define(['jquery', 'core/url'], function($, url) {
    return {
        init: function(selector, insertFunction, ajaxRefreshRate, courseid) {
            var updateText = function(doInsert) {
                $.ajax(url.relativeUrl('/local/customtext/ajax/updatetext.json.php?courseid=' + courseid))
                .done(function(outcome) {
                    if (outcome.success) {
                        if (doInsert) {
                            $(selector)[insertFunction]($('<div data-local_customtext="true">'));
                        }
                        $('[data-local_customtext]').html(outcome.response.text);
                    }
                })
                .fail(function() {
                    // It is fine, just do nothing.
                    return;
                });
            };

            updateText(true);
            if (ajaxRefreshRate > 0) {
                setInterval(() => updateText(false), ajaxRefreshRate * 1000);
            }
        }
    };
});
