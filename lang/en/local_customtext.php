<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_customtext', language 'en'
 * @package    local_customtext
 * @copyright  Astor Bizard, 2023
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Custom text';

$string['privacy:metadata'] = 'This plugin stores no personal data.';

$string['setting:ajaxrefreshrate'] = 'Refresh rate (in seconds)';
$string['setting:ajaxrefreshrate_desc'] = 'Refresh rate for the text, by ajax call. 0 means no refresh (only load on page load). If the text contains no query, leave this field to 0.';
$string['setting:insertafter'] = 'Insert after';
$string['setting:insertappend'] = 'Append';
$string['setting:insertbefore'] = 'Insert before';
$string['setting:inserthtml'] = 'Replace contents';
$string['setting:insertionplace'] = 'Relative position';
$string['setting:insertionplace_desc'] = 'Where to insert the text relatively to the selected element';
$string['setting:insertionselector'] = 'Insert text next to';
$string['setting:insertionselector_desc'] = 'Valid CSS selector pointing to an element on the page. If several elements are selected, the text will be inserted near every selected element.';
$string['setting:insertprepend'] = 'Prepend';
$string['setting:text'] = 'Text';
$string['setting:text_desc'] = 'Text to display.<br>
Database queries can be done by inserting valid SQL queries in double brackets: {{SELECT id FROM ...}}.<br>
These placeholders will be directly substituted with the first column of the first line of the query result.<br>
Standard moodle formatting for queries is recommended (wrap table names in {}).<br>
Available parameters: <code class="text-primary">:now</code> contains current unix timestamp.<br>
The queries results are cached and refreshed once per hour by default.';

$string['update_cached_text_task'] = 'Update cached text for custom text display';
