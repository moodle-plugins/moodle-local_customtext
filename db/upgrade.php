<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade steps definition for local_customtext.
 * @package    local_customtext
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Upgrade steps for local_customtext.
 * @param number $oldversion
 */
function xmldb_local_customtext_upgrade($oldversion = 0) {
    global $DB, $CFG;

    $dbman = $DB->get_manager();

    if ($oldversion < 2024111400) {

        // Define table local_customtext to be created.
        $table = new xmldb_table('local_customtext');

        // Adding fields to table local_customtext.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('text', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table local_customtext.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for local_customtext.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Initiate cache.
        require_once($CFG->dirroot . '/local/customtext/locallib.php');
        $DB->insert_record('local_customtext', [ 'text' => local_customtext_get_text(false) ]);

        // Customtext savepoint reached.
        upgrade_plugin_savepoint(true, 2024111400, 'local', 'customtext');
    }

    return true;
}
