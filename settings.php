<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration plugin-specific settings definition.
 * @package    local_customtext
 * @copyright  Astor Bizard, 2023
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/local/customtext/locallib.php'); // For updatedcallback.

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_customtext', get_string('pluginname', 'local_customtext'));

    $textsetting = new admin_setting_confightmleditor(
            'local_customtext/text',
            get_string('setting:text', 'local_customtext'),
            get_string('setting:text_desc', 'local_customtext'),
            ''
            );
    $textsetting->set_updatedcallback('local_customtext_update_text_cache');
    $settings->add($textsetting);

    $settings->add(new admin_setting_configtext(
            'local_customtext/ajaxrefreshrate',
            get_string('setting:ajaxrefreshrate', 'local_customtext'),
            get_string('setting:ajaxrefreshrate_desc', 'local_customtext'),
            0,
            PARAM_INT
            ));

    $settings->add(new admin_setting_configtext(
            'local_customtext/insertionselector',
            get_string('setting:insertionselector', 'local_customtext'),
            get_string('setting:insertionselector_desc', 'local_customtext'),
            'footer .container',
            PARAM_RAW,
            60
            ));
    $settings->add(new admin_setting_configselect(
            'local_customtext/insertionplace',
            get_string('setting:insertionplace', 'local_customtext'),
            get_string('setting:insertionplace_desc', 'local_customtext'),
            'prepend',
            array(
                    'prepend' => get_string('setting:insertprepend', 'local_customtext'),
                    'append' => get_string('setting:insertappend', 'local_customtext'),
                    'before' => get_string('setting:insertbefore', 'local_customtext'),
                    'after' => get_string('setting:insertafter', 'local_customtext'),
                    'html' => get_string('setting:inserthtml', 'local_customtext')
            )
            ));
    $ADMIN->add('localplugins', $settings);
}
