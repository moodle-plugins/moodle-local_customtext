<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax script to retrieve up-to-date text (as result of queries may have changed).
 *
 * @package    local_customtext
 * @copyright  Astor Bizard, 2023
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define( 'AJAX_SCRIPT', true );

require_once(__DIR__ . '/../../../config.php');
require_once(__DIR__ . '/../locallib.php');

require_login();

global $PAGE;

$outcome = new stdClass();
$outcome->success = true;
$outcome->response = new stdClass();
$outcome->error = '';
try {
    $courseid = required_param('courseid', PARAM_INT);
    $PAGE->set_course(get_course($courseid));
    $outcome->response->text = format_text(local_customtext_get_text(), FORMAT_MOODLE, [ 'para' => false ]);
} catch ( Exception $e ) {
    $outcome->success = false;
    $outcome->error = $e->getMessage();
}
echo json_encode( $outcome );
die();
