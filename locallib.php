<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local lib for this plugin.
 * @package    local_customtext
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Performs database queries to build text to be displayed by this plugin.
 * @param bool $usecache Whether the text should be retrieved from DB cache.
 * @return string HTML fragment. Should be formatted via format_text() or format_string() before display.
 */
function local_customtext_get_text($usecache = true) {
    global $DB;

    if ($usecache && $cached = $DB->get_record('local_customtext', [ 'id' => 1 ])) {
        $text = $cached->text;
    } else {
        $text = get_config('local_customtext', 'text') ?: '';
        $text = preg_replace_callback('/{{(.*?)}}/s', function($match) use($DB) {
            $query = strip_tags(html_entity_decode(preg_replace('#<br ?/?>|&nbsp;#', ' ', $match[1])));
            $res = $DB->get_records_sql($query, [ 'now' => time() ], 0, 1);
            $res = (array)reset($res);
            return reset($res);
        }, $text);
    }
    return $text;
}

/**
 * Update the custom text DB cache.
 * Called by scheduled task and every time the text admin setting changes.
 */
function local_customtext_update_text_cache() {
    global $DB;
    $text = local_customtext_get_text(false);
    if (!$DB->record_exists('local_customtext', [ 'id' => 1 ])) {
        $DB->insert_record('local_customtext', [ 'text' => $text ]);
    } else {
        $DB->update_record('local_customtext', [ 'id' => 1, 'text' => $text ]);
    }
}
